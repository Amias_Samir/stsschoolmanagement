package com.example.adsl4.stschoolmanagement.attendanceteacherdetails;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.adsl4.stschoolmanagement.R;
import com.example.adsl4.stschoolmanagement.eventbus.AttendanceStudentSelectEvent;
import com.example.adsl4.stschoolmanagement.teacherattendance.AttendanceDetails;
import com.example.adsl4.stschoolmanagement.teacherattendance.StudentAttendance;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class DaillyAttendanceListAdapter extends BaseQuickAdapter<DayWiseAttendanceListResponse, BaseViewHolder> {

    Context context;

    public DaillyAttendanceListAdapter(int layoutResId, @Nullable List<DayWiseAttendanceListResponse> data) {
        super(layoutResId, data);
    }


    @Override
    protected void convert(final BaseViewHolder helper, final DayWiseAttendanceListResponse item) {
        helper.setText(R.id.txtStudentName, item.getStudentName());
//        helper.setText(R.id.txtNoticeDate,item.getCreatedDate().toString());
//        helper.setText(R.id.txtNoticeShort,item.getShortDesc());
        final CheckBox cbAttendancePresent = helper.getView(R.id.chkAttendancePresent);
        final CheckBox cbAttendanceAbsent = helper.getView(R.id.chkAttendanceAbsent);

        if(item.getStudentStatus().equals("Present")){
            cbAttendancePresent.setChecked(true);
            cbAttendanceAbsent.setChecked(false);
        }else {
            cbAttendanceAbsent.setChecked(true);
            cbAttendancePresent.setChecked(false);
        }
        cbAttendanceAbsent.setEnabled(false);
        cbAttendancePresent.setEnabled(false);
    }


}