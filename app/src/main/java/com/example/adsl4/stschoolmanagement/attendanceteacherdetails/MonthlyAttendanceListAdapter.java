package com.example.adsl4.stschoolmanagement.attendanceteacherdetails;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.adsl4.stschoolmanagement.R;
import com.example.adsl4.stschoolmanagement.assignedteacherassignment.AsgndAsgmntDtlsActivity;

import java.util.List;

public class MonthlyAttendanceListAdapter extends BaseQuickAdapter<AttendancesListResponse, BaseViewHolder> {

    Context context;

    public MonthlyAttendanceListAdapter(int layoutResId, @Nullable List<AttendancesListResponse> data) {
        super(layoutResId, data);
    }


    @Override
    protected void convert(final BaseViewHolder helper, final AttendancesListResponse item) {
//        helper.setText(R.id.txtAssignmentNo, "QNo. "+String.valueOf(item.));

        try {
            helper.setText(R.id.txtAttendanceNo, helper.getAdapterPosition() + "");
            if(item.getDay() != null && item.getDay().length() >=3) {
                helper.setText(R.id.txtAttendanceDay, item.getDay().substring(0, Math.min(item.getDay().length(), 3)));
            }else {
                helper.setText(R.id.txtAttendanceDay, item.getDay()+"");
            }
            helper.setText(R.id.txtAttendanceClass, item.getClasses()+"");
            helper.setText(R.id.txtAttendanceSection, item.getSection()+"");

            String realdate = item.getAttendanceDate();
            String[] splitDate = realdate.split("T");
            String attendanceDate = splitDate[0];
            helper.setText(R.id.txtAttendanceDate, attendanceDate);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        helper.getView(R.id.btnViewAttendanceDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(helper.getView(R.id.btnViewAttendanceDetails).getContext(), DailyAttndnceDtlsActivity.class);
                intent.putExtra("item", item);
                helper.getView(R.id.btnViewAttendanceDetails).getContext().startActivity(intent);
            }
        });

    }


}
