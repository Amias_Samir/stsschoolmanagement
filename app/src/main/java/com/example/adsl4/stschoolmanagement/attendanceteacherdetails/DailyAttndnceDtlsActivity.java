package com.example.adsl4.stschoolmanagement.attendanceteacherdetails;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adsl4.stschoolmanagement.R;
import com.example.adsl4.stschoolmanagement.login.TeacherDetailsResponse;
import com.example.adsl4.stschoolmanagement.network.NetworkApiClient;
import com.example.adsl4.stschoolmanagement.network.NetworkApiInterface;
import com.example.adsl4.stschoolmanagement.teacherattendance.TeacherAttendanceAdapter;
import com.example.adsl4.stschoolmanagement.utils.JsonAndGsonOperation;
import com.example.adsl4.stschoolmanagement.utils.SharedPreferenceUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class DailyAttndnceDtlsActivity extends AppCompatActivity {

    private static final String TAG = "DailyAttndnceDtls" ;
    @BindView(R.id.prgTeacherAssignment)
    ProgressBar prgTeacherAssignment;
    @BindView(R.id.tv_no_data_found)
    TextView tvNoDataFound;
    @BindView(R.id.recyclerStudentList)
    RecyclerView recyclerStudentList;

    TeacherDetailsResponse teacherDetailsResponse;
    AttendancesListResponse attendancesListResponse ;

    List <DayWiseAttendanceListResponse> dayWiseAttendanceListResponseList = new ArrayList<DayWiseAttendanceListResponse>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_attndnce_dtls);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

        recyclerStudentList.setHasFixedSize(true);
        recyclerStudentList.setLayoutManager(new LinearLayoutManager(this));

        teacherDetailsResponse = JsonAndGsonOperation.getTeacherDetails(DailyAttndnceDtlsActivity.this);
        setupListRecycler();

        try {
            attendancesListResponse = getIntent().getExtras().getParcelable("item");
            getSupportActionBar().setTitle(attendancesListResponse.getDay()+"'s Attendance");

            fetchAttendanceFromServer();
        }catch (NullPointerException e){
            e.printStackTrace();
        }





    }

    private void setupListRecycler() {
        DaillyAttendanceListAdapter daillyAttendanceListAdapter = new DaillyAttendanceListAdapter( R.layout.teacher_attendance_list_item_layout, null);
        recyclerStudentList.setLayoutManager(new LinearLayoutManager(this));
        recyclerStudentList.setAdapter(daillyAttendanceListAdapter);
    }

    public void fetchAttendanceFromServer(){
        prgTeacherAssignment.setVisibility(View.VISIBLE);
        NetworkApiInterface apiService = NetworkApiClient.getAPIClient().create(NetworkApiInterface.class);
        Log.d(TAG, "FetchAttendance: method  ");

        apiService.getDailyAttendanceDetails(teacherDetailsResponse.getOrganizationId(), teacherDetailsResponse.getBranchId(),
                attendancesListResponse.getAttendanceId())
//        apiService.getDailyAttendanceDetails(10, 9, attendancesListResponse.getAttendanceId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<List<DayWiseAttendanceListResponse>>(){
                    @Override
                    public void onNext(List<DayWiseAttendanceListResponse> dayWiseAttendanceListResponses) {
                        dayWiseAttendanceListResponseList.addAll(dayWiseAttendanceListResponses);
                        Log.d(TAG, "onNext: "+dayWiseAttendanceListResponseList.size());
                    }

                    @Override
                    public void onError(Throwable e) {
                        prgTeacherAssignment.setVisibility(View.GONE);
                        Log.d(TAG, "FetchAttendance: error message "+e.getMessage());
                        Log.d(TAG, "FetchAttendance: error cause "+e.getCause());
                        Log.d(TAG, "FetchAttendance: error "+e.toString());

                        tvNoDataFound.setText(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        prgTeacherAssignment.setVisibility(View.GONE);
                        Log.d(TAG, "onComplete: "+dayWiseAttendanceListResponseList.size());
                        try {
                        if (dayWiseAttendanceListResponseList.size() <= 0) {
                            prgTeacherAssignment.setVisibility(View.GONE);
                            tvNoDataFound.setVisibility(View.VISIBLE);
                            Toast.makeText(DailyAttndnceDtlsActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        tvNoDataFound.setVisibility(View.GONE);
                        prgTeacherAssignment.setVisibility(View.GONE);
                        ((DaillyAttendanceListAdapter) recyclerStudentList.getAdapter()).replaceData(dayWiseAttendanceListResponseList);

                    } catch (Exception e) {
//                            tvNoDataFound.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();  return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }
}
