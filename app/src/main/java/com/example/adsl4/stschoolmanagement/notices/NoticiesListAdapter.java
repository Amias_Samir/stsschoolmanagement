package com.example.adsl4.stschoolmanagement.notices;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.adsl4.stschoolmanagement.R;
import com.example.adsl4.stschoolmanagement.eventbus.NoticeItemSelectEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class NoticiesListAdapter extends BaseQuickAdapter<StudentNoticeModal, BaseViewHolder> {


    public NoticiesListAdapter(int layoutResId, @Nullable List<StudentNoticeModal> data) {
        super(layoutResId, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, final StudentNoticeModal item) {
        helper.setText(R.id.txtNoticeHead,item.getDisplayName());
        try {

            String realdate= item.getCreatedDate();
            Log.e("StudentMessageAdapter", "onBindViewHolder: "+realdate );
            String[] splitDate=realdate.split("T");
            String noticeDate="Notice Date: "+splitDate[0];
            helper.setText(R.id.txtNoticeDate, noticeDate);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        helper.setText(R.id.txtNoticeShort,item.getShortDesc());
        helper.getView(R.id.lnrStsNotice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new NoticeItemSelectEvent.NoticeItemClick(item));
            }
        });


    }



}