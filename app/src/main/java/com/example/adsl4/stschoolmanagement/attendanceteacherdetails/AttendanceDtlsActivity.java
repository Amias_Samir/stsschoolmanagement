package com.example.adsl4.stschoolmanagement.attendanceteacherdetails;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.adsl4.stschoolmanagement.R;
import com.example.adsl4.stschoolmanagement.activities.AssignmentTeacher;
import com.example.adsl4.stschoolmanagement.activities.DashboardTeacher;
import com.example.adsl4.stschoolmanagement.activities.StudentAttendance;
import com.example.adsl4.stschoolmanagement.adapters.LIstItemAssignmentBatch;
import com.example.adsl4.stschoolmanagement.adapters.LIstItemAssignmentClass;
import com.example.adsl4.stschoolmanagement.adapters.LIstItemAssignmentSection;
import com.example.adsl4.stschoolmanagement.adapters.LIstItemAssignmentSubjects;
import com.example.adsl4.stschoolmanagement.api.GetBatchAsignmentApi;
import com.example.adsl4.stschoolmanagement.api.GetClassAsignmentApi;
import com.example.adsl4.stschoolmanagement.api.GetSectionAsignmentApi;
import com.example.adsl4.stschoolmanagement.api.MonthDetailApi;
import com.example.adsl4.stschoolmanagement.assignedteacherassignment.AssignedAssgnmntActivity;
import com.example.adsl4.stschoolmanagement.assignedteacherassignment.AssignedAssignmentListAdapter;
import com.example.adsl4.stschoolmanagement.login.TeacherDetailsResponse;
import com.example.adsl4.stschoolmanagement.modals.GetBatchAsignmentModal;
import com.example.adsl4.stschoolmanagement.modals.GetClassAsignmentModal;
import com.example.adsl4.stschoolmanagement.modals.GetSectionAsignmentModal;
import com.example.adsl4.stschoolmanagement.modals.MonthDetailModal;
import com.example.adsl4.stschoolmanagement.network.NetworkApiClient;
import com.example.adsl4.stschoolmanagement.network.NetworkApiInterface;
import com.example.adsl4.stschoolmanagement.utils.JsonAndGsonOperation;
import com.example.adsl4.stschoolmanagement.utils.SharedPreferenceUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AttendanceDtlsActivity extends AppCompatActivity {

    @BindView(R.id.prgTeacherAssignment)
    ProgressBar prgTeacherAssignment;
    @BindView(R.id.txtBatch)
    TextView txtBatch;
    @BindView(R.id.spnAssignmentBatch)
    Spinner spnAssignmentBatch;
    @BindView(R.id.spnAssignmentGrade)
    Spinner spnAssignmentGrade;
    @BindView(R.id.spnAssignmentSection)
    Spinner spnAssignmentSection;
    @BindView(R.id.spnGetMonth)
    Spinner spnGetMonth;
    @BindView(R.id.teacher_attendance_list_recycler_view)
    RecyclerView teacherAttendanceListRecyclerView;
    @BindView(R.id.tv_no_data_found)
    TextView tvNoDataFound;
    @BindView(R.id.relMain)
    RelativeLayout relMain;


    String Student, Organization, batchName, className, sectionName, subjectName;
    int orgId, branchId, batchId, bthId, classId, clsid, sectionId, secId, subjectId, subId;
    int monthNumber, attenDay, monthNum;

    private List<AttendancesListResponse> attendancesListResponseList = new ArrayList<AttendancesListResponse>();
    private List<LIstItemAssignmentBatch> lIstItemAssignmentBatches;
    private List<LIstItemAssignmentClass> lIstItemAssignmentClasses;
    private List<LIstItemAssignmentSection> lIstItemAssignmentSections;
    private List<MonthDetailModal> listMonthDetails;

    SharedPreferenceUtils sharedPreferenceUtils;
    TeacherDetailsResponse teacherDetailsResponse;
    private static final String TAG = "AttendanceDtls";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_dtls);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

        sharedPreferenceUtils = new SharedPreferenceUtils(AttendanceDtlsActivity.this);
        teacherDetailsResponse = JsonAndGsonOperation.getTeacherDetails(AttendanceDtlsActivity.this);

        RelativeLayout myLayout = this.findViewById(R.id.relMain);
        myLayout.requestFocus();
        setupListRecycler();
        getBatch();
    }

    private void setupListRecycler() {
        MonthlyAttendanceListAdapter monthlyAttendanceListAdapter = new MonthlyAttendanceListAdapter(R.layout.monthly_attendance_list_item_layout, null);
        teacherAttendanceListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        teacherAttendanceListRecyclerView.setAdapter(monthlyAttendanceListAdapter);
    }


    public void getBatch() {
        prgTeacherAssignment.setVisibility(View.VISIBLE);

        orgId = teacherDetailsResponse.getOrganizationId();
        branchId = teacherDetailsResponse.getBranchId();
        final Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(getString(R.string.get_Batch))
                .build();
        final GetBatchAsignmentApi getBatchAsignmentApi = retrofit.create(GetBatchAsignmentApi.class);
        Call<List<GetBatchAsignmentModal>> batchs = getBatchAsignmentApi.getBatchDetail(orgId, branchId);
        batchs.enqueue(new Callback<List<GetBatchAsignmentModal>>() {
            @Override
            public void onResponse(Call<List<GetBatchAsignmentModal>> call, Response<List<GetBatchAsignmentModal>> response) {
//                HttpUrl test=call.request().url();
//                for (int i=0;i<10;i++) {
//                    Toast.makeText(AssignmentTeacher.this, "Error " + test, Toast.LENGTH_SHORT).show();
//                }
                List<GetBatchAsignmentModal> getBatchAsignmentModals = response.body();
                lIstItemAssignmentBatches = new ArrayList<>();
                lIstItemAssignmentBatches.add(new LIstItemAssignmentBatch(0, "Select Batch"));
//                String hello=new GsonBuilder().setPrettyPrinting().create().toJson(response);
//                txtBatch.setText(hello);
                for (int i = 0; i < getBatchAsignmentModals.size(); i++) {
                    batchId = getBatchAsignmentModals.get(i).getBatchId();
                    batchName = getBatchAsignmentModals.get(i).getBatchName();
                    LIstItemAssignmentBatch item = new LIstItemAssignmentBatch(batchId, batchName);
                    lIstItemAssignmentBatches.add(item);
                }
                ArrayAdapter<LIstItemAssignmentBatch> assignmentBatchArrayAdapter = new ArrayAdapter<LIstItemAssignmentBatch>(AttendanceDtlsActivity.this,
                        android.R.layout.simple_spinner_item, lIstItemAssignmentBatches);
                assignmentBatchArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnAssignmentBatch.setAdapter(assignmentBatchArrayAdapter);
                prgTeacherAssignment.setVisibility(View.GONE);
                spnAssignmentBatch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        prgTeacherAssignment.setVisibility(View.GONE);

                        try {
                            if (spnAssignmentBatch.getSelectedItemId() > 0) {
//                            prgTeacherAssignment.setVisibility(View.VISIBLE);
                                LIstItemAssignmentBatch lIstItemAssignmentBatch = (LIstItemAssignmentBatch) spnAssignmentBatch.getSelectedItem();
                                bthId = lIstItemAssignmentBatch.getBatchsId();
                                ///Toast.makeText(AssignmentTeacher.this, "ID: " + bthId, Toast.LENGTH_SHORT).show();

                                getGrades();
                            }
                        } catch (Exception ex) {
                            Toast.makeText(AttendanceDtlsActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onFailure(Call<List<GetBatchAsignmentModal>> call, Throwable t) {
                prgTeacherAssignment.setVisibility(View.GONE);
                HttpUrl test = call.request().url();
                Toast.makeText(AttendanceDtlsActivity.this, "Error " + test, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getGrades() {
        prgTeacherAssignment.setVisibility(View.VISIBLE);

        orgId = teacherDetailsResponse.getOrganizationId();
        branchId = teacherDetailsResponse.getBranchId();
        final Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(getString(R.string.get_class_url))
                .build();
        final GetClassAsignmentApi getClassAsignmentApi = retrofit.create(GetClassAsignmentApi.class);
        Call<List<GetClassAsignmentModal>> classes = getClassAsignmentApi.getClassDetail(orgId, branchId, bthId);
        classes.enqueue(new Callback<List<GetClassAsignmentModal>>() {
            @Override
            public void onResponse(Call<List<GetClassAsignmentModal>> call, Response<List<GetClassAsignmentModal>> response) {

                List<GetClassAsignmentModal> getClassAsignmentModalList = response.body();
                lIstItemAssignmentClasses = new ArrayList<>();
                lIstItemAssignmentClasses.add(new LIstItemAssignmentClass(0, "Select Class"));

                for (int i = 0; i < getClassAsignmentModalList.size(); i++) {
                    classId = getClassAsignmentModalList.get(i).getClassId();
                    className = getClassAsignmentModalList.get(i).getClassName();
                    LIstItemAssignmentClass item = new LIstItemAssignmentClass(classId, className);
                    lIstItemAssignmentClasses.add(item);
                }
                ArrayAdapter<LIstItemAssignmentClass> assignmentClassArrayAdapter = new ArrayAdapter<LIstItemAssignmentClass>(AttendanceDtlsActivity.this,
                        android.R.layout.simple_spinner_item, lIstItemAssignmentClasses);
                assignmentClassArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnAssignmentGrade.setAdapter(assignmentClassArrayAdapter);
                spnAssignmentGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        prgTeacherAssignment.setVisibility(View.GONE);

                        try {

                            if (spnAssignmentGrade.getSelectedItemId() > 0) {
                                LIstItemAssignmentClass lIstItemAssignmentClass = (LIstItemAssignmentClass) spnAssignmentGrade.getSelectedItem();
                                clsid = lIstItemAssignmentClass.getClassId();
                                ///Toast.makeText(AssignmentTeacher.this, "ID: " + clsid, Toast.LENGTH_SHORT).show();
                                getSections();
                            }
                        } catch (Exception ex) {
                            Toast.makeText(AttendanceDtlsActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onFailure(Call<List<GetClassAsignmentModal>> call, Throwable t) {
                prgTeacherAssignment.setVisibility(View.GONE);

                HttpUrl test = call.request().url();
                Toast.makeText(AttendanceDtlsActivity.this, "Error " + test, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getSections() {
        prgTeacherAssignment.setVisibility(View.VISIBLE);

        orgId = teacherDetailsResponse.getOrganizationId();
        branchId = teacherDetailsResponse.getBranchId();
        final Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(getString(R.string.get_section_url))
                .build();
        final GetSectionAsignmentApi getSectionAsignmentApi = retrofit.create(GetSectionAsignmentApi.class);
        Call<List<GetSectionAsignmentModal>> classes = getSectionAsignmentApi.getSectionDetail(orgId, branchId, bthId, clsid);
        classes.enqueue(new Callback<List<GetSectionAsignmentModal>>() {
            @Override
            public void onResponse(Call<List<GetSectionAsignmentModal>> call, Response<List<GetSectionAsignmentModal>> response) {

                List<GetSectionAsignmentModal> getSectionAsignmentModalList = response.body();
                lIstItemAssignmentSections = new ArrayList<>();
                lIstItemAssignmentSections.add(new LIstItemAssignmentSection(0, "Select Section"));

                for (int i = 0; i < getSectionAsignmentModalList.size(); i++) {
                    sectionId = getSectionAsignmentModalList.get(i).getSectionId();
                    sectionName = getSectionAsignmentModalList.get(i).getSectionName();
                    LIstItemAssignmentSection item = new LIstItemAssignmentSection(sectionId, sectionName);
                    lIstItemAssignmentSections.add(item);
                }
                ArrayAdapter<LIstItemAssignmentSection> assignmentSectionArrayAdapter = new ArrayAdapter<LIstItemAssignmentSection>(AttendanceDtlsActivity.this,
                        android.R.layout.simple_spinner_item, lIstItemAssignmentSections);
                assignmentSectionArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnAssignmentSection.setAdapter(assignmentSectionArrayAdapter);
                spnAssignmentSection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        prgTeacherAssignment.setVisibility(View.GONE);

                        try {

                            if (spnAssignmentSection.getSelectedItemId() > 0) {
                                LIstItemAssignmentSection lIstItemAssignmentSection = (LIstItemAssignmentSection) spnAssignmentSection.getSelectedItem();
                                secId = lIstItemAssignmentSection.getBatchsId();
                                getMonths();
                            }
                        } catch (Exception ex) {
                            secId = 0;
                            Toast.makeText(AttendanceDtlsActivity.this, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onFailure(Call<List<GetSectionAsignmentModal>> call, Throwable t) {
                prgTeacherAssignment.setVisibility(View.GONE);

                HttpUrl test = call.request().url();
                Toast.makeText(AttendanceDtlsActivity.this, "Error " + test, Toast.LENGTH_SHORT).show();
            }
        });
    }

    ArrayAdapter<MonthDetailModal> monthListArrayAdapter;

    public void getMonths() {
        prgTeacherAssignment.setVisibility(View.VISIBLE);

        final Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(getString(R.string.get_month_url))
                .build();
        final MonthDetailApi monthDetailApi = retrofit.create(MonthDetailApi.class);
        Call<List<MonthDetailModal>> months = monthDetailApi.getMonths();
        months.enqueue(new Callback<List<MonthDetailModal>>() {
            @Override
            public void onResponse(Call<List<MonthDetailModal>> call, Response<List<MonthDetailModal>> response) {
                List<MonthDetailModal> monthDetailModals = response.body();
                monthNumber = monthDetailModals.get(monthDetailModals.size() - 1).getMonthNumber();
                spnGetMonth.setSelection(monthNumber);

                if (monthDetailModals.size() <= 0) {
                    Toast.makeText(AttendanceDtlsActivity.this, "Empty Month List", Toast.LENGTH_SHORT).show();
                }
                prgTeacherAssignment.setVisibility(View.GONE);


                monthListArrayAdapter = new ArrayAdapter<MonthDetailModal>(AttendanceDtlsActivity.this,
                        android.R.layout.simple_spinner_item, monthDetailModals);
                monthListArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnGetMonth.setAdapter(monthListArrayAdapter);

                spnGetMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        prgTeacherAssignment.setVisibility(View.GONE);

                        monthNum = monthListArrayAdapter.getItem(spnGetMonth.getSelectedItemPosition()).getMonthNumber();
                        fetchAttendanceFromServer();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<MonthDetailModal>> call, Throwable t) {
                prgTeacherAssignment.setVisibility(View.GONE);

                Toast.makeText(AttendanceDtlsActivity.this, "Error GetMonth: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void fetchAttendanceFromServer() {
        prgTeacherAssignment.setVisibility(View.VISIBLE);

        NetworkApiInterface apiService = NetworkApiClient.getAPIClient().create(NetworkApiInterface.class);
        Log.d(TAG, "FetchAttendance: method  ");

        apiService.getMonthlyAttendanceListDetails(orgId, branchId, bthId, clsid, secId, teacherDetailsResponse.getEmployeeDetailId(), monthNum)
//        apiService.getMonthlyAttendanceListDetails(10,9,10,33,37,46,4)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<List<AttendancesListResponse>>() {

                    @Override
                    public void onNext(List<AttendancesListResponse> attendancesListResponses) {

                        if(attendancesListResponses instanceof JSONObject){
                            prgTeacherAssignment.setVisibility(View.GONE);
                            tvNoDataFound.setVisibility(View.VISIBLE);
                            return;

                        }

                        try {

                            attendancesListResponseList.addAll(attendancesListResponses);
                            Log.d(TAG, "onNext: " + attendancesListResponses.size());
                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        prgTeacherAssignment.setVisibility(View.GONE);
                        tvNoDataFound.setVisibility(View.VISIBLE);
                        attendancesListResponseList = new ArrayList<AttendancesListResponse>();
                        ((MonthlyAttendanceListAdapter) teacherAttendanceListRecyclerView.getAdapter()).replaceData(attendancesListResponseList);

                        Toast.makeText(AttendanceDtlsActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "FetchAttendance: error message " + e.getMessage());
                        Log.d(TAG, "FetchAttendance: error cause " + e.getCause());
                        Log.d(TAG, "FetchAttendance: error " + e.toString());
                    }

                    @Override
                    public void onComplete() {
                        try {
                            prgTeacherAssignment.setVisibility(View.GONE);
                            Log.d(TAG, "onComplete: " + attendancesListResponseList.size());

                            if (attendancesListResponseList.size() <= 0) {
                                prgTeacherAssignment.setVisibility(View.GONE);
                                tvNoDataFound.setVisibility(View.VISIBLE);
                                Toast.makeText(AttendanceDtlsActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            tvNoDataFound.setVisibility(View.GONE);
                            prgTeacherAssignment.setVisibility(View.GONE);
                            ((MonthlyAttendanceListAdapter) teacherAttendanceListRecyclerView.getAdapter()).replaceData(attendancesListResponseList);

                        } catch (Exception e) {
//                            tvNoDataFound.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                        }

                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
//            onBackPressed();
            startActivity(new Intent(AttendanceDtlsActivity.this, DashboardTeacher.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
//
//    @Override
//    public void onBackPressed()
//    {
//        // code here to show dialog
//        super.onBackPressed();  // optional depending on your needs
//    }
}
