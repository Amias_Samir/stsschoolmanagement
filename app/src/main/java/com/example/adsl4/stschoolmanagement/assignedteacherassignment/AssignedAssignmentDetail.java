
package com.example.adsl4.stschoolmanagement.assignedteacherassignment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssignedAssignmentDetail implements Parcelable {

    @SerializedName("assignmentId")
    @Expose
    private Integer assignmentId;
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("branchId")
    @Expose
    private Integer branchId;
    @SerializedName("batchId")
    @Expose
    private Integer batchId;
    @SerializedName("classId")
    @Expose
    private Integer classId;
    @SerializedName("sectionId")
    @Expose
    private Integer sectionId;
    @SerializedName("assignedDate")
    @Expose
    private String assignedDate;
    @SerializedName("employeeDetailId")
    @Expose
    private Integer employeeDetailId;
    @SerializedName("classSubjectId")
    @Expose
    private Integer classSubjectId;
    @SerializedName("assignmentName")
    @Expose
    private String assignmentName;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("updatedBy")
    @Expose
    private String updatedBy;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;
    @SerializedName("approvedBy")
    @Expose
    private String approvedBy;
    @SerializedName("approvedDate")
    @Expose
    private String approvedDate;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("batch")
    @Expose
    private String batch;
    @SerializedName("branch")
    @Expose
    private String branch;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("employeeDetail")
    @Expose
    private String employeeDetail;
    @SerializedName("classSubject")
    @Expose
    private String classSubject;

    public Integer getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Integer assignmentId) {
        this.assignmentId = assignmentId;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getBatchId() {
        return batchId;
    }

    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(String assignedDate) {
        this.assignedDate = assignedDate;
    }

    public Integer getEmployeeDetailId() {
        return employeeDetailId;
    }

    public void setEmployeeDetailId(Integer employeeDetailId) {
        this.employeeDetailId = employeeDetailId;
    }

    public Integer getClassSubjectId() {
        return classSubjectId;
    }

    public void setClassSubjectId(Integer classSubjectId) {
        this.classSubjectId = classSubjectId;
    }

    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getEmployeeDetail() {
        return employeeDetail;
    }

    public void setEmployeeDetail(String employeeDetail) {
        this.employeeDetail = employeeDetail;
    }

    public String getClassSubject() {
        return classSubject;
    }

    public void setClassSubject(String classSubject) {
        this.classSubject = classSubject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.assignmentId);
        dest.writeValue(this.organizationId);
        dest.writeValue(this.branchId);
        dest.writeValue(this.batchId);
        dest.writeValue(this.classId);
        dest.writeValue(this.sectionId);
        dest.writeString(this.assignedDate);
        dest.writeValue(this.employeeDetailId);
        dest.writeValue(this.classSubjectId);
        dest.writeString(this.assignmentName);
        dest.writeString(this.createdBy);
        dest.writeString(this.createdDate);
        dest.writeString(this.updatedBy);
        dest.writeString(this.updatedDate);
        dest.writeString(this.approvedBy);
        dest.writeString(this.approvedDate);
        dest.writeString(this.organization);
        dest.writeString(this.batch);
        dest.writeString(this.branch);
        dest.writeString(this._class);
        dest.writeString(this.section);
        dest.writeString(this.employeeDetail);
        dest.writeString(this.classSubject);
    }

    public AssignedAssignmentDetail() {
    }

    protected AssignedAssignmentDetail(Parcel in) {
        this.assignmentId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.organizationId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.branchId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.batchId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.classId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sectionId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.assignedDate = in.readString();
        this.employeeDetailId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.classSubjectId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.assignmentName = in.readString();
        this.createdBy = in.readString();
        this.createdDate = in.readString();
        this.updatedBy = in.readString();
        this.updatedDate = in.readString();
        this.approvedBy = in.readString();
        this.approvedDate = in.readString();
        this.organization = in.readString();
        this.batch = in.readString();
        this.branch = in.readString();
        this._class = in.readString();
        this.section = in.readString();
        this.employeeDetail = in.readString();
        this.classSubject = in.readString();
    }

    public static final Parcelable.Creator<AssignedAssignmentDetail> CREATOR = new Parcelable.Creator<AssignedAssignmentDetail>() {
        @Override
        public AssignedAssignmentDetail createFromParcel(Parcel source) {
            return new AssignedAssignmentDetail(source);
        }

        @Override
        public AssignedAssignmentDetail[] newArray(int size) {
            return new AssignedAssignmentDetail[size];
        }
    };
}