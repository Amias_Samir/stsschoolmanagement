package com.example.adsl4.stschoolmanagement.assignedteacherassignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.adsl4.stschoolmanagement.R;
import com.example.adsl4.stschoolmanagement.activities.DashboardTeacher;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AsgndAsgmntDtlsActivity extends AppCompatActivity {

    @BindView(R.id.txtHomeworkNo)
    TextView txtHomeworkNo;
    @BindView(R.id.txtHomeworkBody)
    TextView txtHomeworkBody;
    @BindView(R.id.txtHomeworkDate)
    TextView txtHomeworkDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asgnd_asgmnt_dtls);
        ButterKnife.bind(this);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        getSupportActionBar().setTitle("Assigned Assignment");

//        Bundle data = getIntent().getExtras();
//        AssignedAssignmentDetail assignedAssignmentDetail = (AssignedAssignmentDetail) data.getParcelable("item");
        AssignedAssignmentDetail assignedAssignmentDetail = getIntent().getExtras().getParcelable("item");

        try {
            txtHomeworkNo.setText(assignedAssignmentDetail.getAssignmentId()+"");
            txtHomeworkBody.setText(Html.fromHtml(assignedAssignmentDetail.getAssignmentName()));


            String realdate= assignedAssignmentDetail.getAssignedDate();
            String[] splitDate=realdate.split("T");
            String assignmentDate="Assignment Date: "+splitDate[0];
            txtHomeworkDate.setText(assignmentDate);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
//            startActivity(new Intent(AsgndAsgmntDtlsActivity.this, AssignedAssgnmntActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }
}
