package com.example.adsl4.stschoolmanagement.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adsl4.stschoolmanagement.R;
import com.example.adsl4.stschoolmanagement.api.MessageToAdmin;
import com.example.adsl4.stschoolmanagement.login.StudentDetail;
import com.example.adsl4.stschoolmanagement.login.TeacherDetailsResponse;
import com.example.adsl4.stschoolmanagement.modals.MessageToAdminModal;
import com.example.adsl4.stschoolmanagement.utils.JsonAndGsonOperation;
import com.example.adsl4.stschoolmanagement.utils.SharedPreferenceUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StudentMessageDetail extends AppCompatActivity {
    TextView txtMessageDetailHead,txtMessageDetailBody,txtMessageDetailDate, textMessageDetailMessageBy;
    EditText edtMessageBody;
    Button btnReplySend;
    String  Student,Organization;
    TeacherDetailsResponse teacherDetailsResponse;
    StudentDetail studentDetail;
    SharedPreferenceUtils sharedPreferenceUtils;

    String usrId = "";
    String msgFromUsr= "";
    int batchId= 0 ;

    int branchId= 0 ;
    int organizationId= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_message_detail);

        txtMessageDetailHead=findViewById(R.id.txtMessageDetailHead);
        txtMessageDetailBody=findViewById(R.id.txtMessageDetailBody);
        txtMessageDetailDate=findViewById(R.id.txtMessageDetailDate);
        textMessageDetailMessageBy=findViewById(R.id.txtMessageDetailMessageBy);

        sharedPreferenceUtils = new SharedPreferenceUtils(StudentMessageDetail.this);
        if((sharedPreferenceUtils.getIntValue(SharedPreferenceUtils.KEY_USER_ID, -1)) == 0){
            studentDetail = JsonAndGsonOperation.getStudentDetails(StudentMessageDetail.this);

            usrId = studentDetail.getUserId();
            batchId = studentDetail.getBatchId();
            branchId = studentDetail.getBranchId();
            organizationId = studentDetail.getOrganizationId();

        }else if((sharedPreferenceUtils.getIntValue(SharedPreferenceUtils.KEY_USER_ID, -1)) == 0){
            teacherDetailsResponse = JsonAndGsonOperation.getTeacherDetails(StudentMessageDetail.this);

            usrId = teacherDetailsResponse.getUserId();
//            batchId = teacherDetailsResponse.getB();
            branchId = teacherDetailsResponse.getBranchId();
            organizationId = teacherDetailsResponse.getOrganizationId();
        }

        Intent intent=getIntent();
        txtMessageDetailHead.setText(intent.getStringExtra("messageHead"));
        txtMessageDetailBody.setText(intent.getStringExtra("messageBody"));
        txtMessageDetailDate.setText(intent.getStringExtra("messageDate"));
        textMessageDetailMessageBy.setText("Message from : "+intent.getStringExtra("messageBy"));
        msgFromUsr = intent.getStringExtra("messageByID");

        edtMessageBody=findViewById(R.id.edtMessageBody);
        btnReplySend=findViewById(R.id.btnReplySend);
        btnReplySend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date currentTime = Calendar.getInstance().getTime();

                Date date = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String formattedDate = df.format(date);



                MessageToAdminModal messageToAdminModal= new MessageToAdminModal(
                        0,msgFromUsr,"Reply Message",null,null,null,null,"Reply Message",edtMessageBody.getText().toString(),
                        usrId,null,0,organizationId,branchId,null,null,null,msgFromUsr,
                        formattedDate,null,null,null,null,null,null,null,null,null,null,null,batchId);

                Gson gson = new Gson();
                String jsonString = gson.toJson(messageToAdminModal);

                replyToAdmin(messageToAdminModal);
            }
        });
    }
    public void replyToAdmin(MessageToAdminModal messageToAdminModal){
        Retrofit retrofit=new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://mobile.sheshayapathshala.com.np/api/StudentDetails/")
                .build();
        final MessageToAdmin messageToAdmin = retrofit.create(MessageToAdmin.class);
        Call<List<MessageToAdminModal>> messages=messageToAdmin.sendMessage(messageToAdminModal);
        messages.enqueue(new Callback<List<MessageToAdminModal>>() {
            @Override
            public void onResponse(Call<List<MessageToAdminModal>> call, Response<List<MessageToAdminModal>> response) {
                String hello=new GsonBuilder().setPrettyPrinting().create().toJson(response.body());
                Log.w("gson => ",new GsonBuilder().setPrettyPrinting().create().toJson(response));
//                for (int i=0;i<10;i++) {
                    Toast.makeText(StudentMessageDetail.this, "Replied Successfully" , Toast.LENGTH_LONG).show();
//                }
                 startActivity(new Intent(StudentMessageDetail.this, StudentMessage.class));
                finish();
//                txtMessageDetailBody.setText(hello);
            }

            @Override
            public void onFailure(Call<List<MessageToAdminModal>> call, Throwable t) {
                Toast.makeText(StudentMessageDetail.this, "Error: " +t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }
}
